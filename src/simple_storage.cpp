#include <eosio/eosio.hpp>
#include <eosio/print.hpp>
#include <eosio/crypto.hpp>



class [[eosio::contract]] simple_storage : public eosio::contract {
  public:
      simple_storage(eosio::name receiver, eosio::name code, eosio::datastream<const char*> ds ):
         eosio::contract(receiver, code, ds),
         _vals(receiver, code.value)
      {}

      struct [[eosio::table]] kvp {
         uint64_t key;
         std::string value;

         uint64_t primary_key() const { return key; }
      };

      typedef eosio::multi_index<"valstable"_n, kvp> valstable;
      
      valstable _vals;

      [[eosio::action]]
      void setval(uint64_t key, std::string value) {
         _vals.emplace(get_self(), [&](auto& kvp) {
            kvp.key = key;
            kvp.value = value;
         });
      }

      [[eosio::action]]
      void getval(uint64_t key) {
         auto kvp = _vals.get(key);
         eosio::print(kvp.value);
      }
};
