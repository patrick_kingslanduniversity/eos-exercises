import sys
from eosfactory.eosf import *

verbosity([Verbosity.INFO, Verbosity.OUT, Verbosity.DEBUG])

CONTRACT_WORKSPACE = sys.path[0] + "/../"

# Actors of the test:
MASTER = MasterAccount()
HOST = Account()
ALICE = Account()

def test():
    SCENARIO('''
    Execute simple actions.
    ''')
    reset()
    create_master_account("MASTER")

    COMMENT('''
    Build and deploy the contract:
    ''')
    create_account("HOST", MASTER)
    smart = Contract(HOST, CONTRACT_WORKSPACE)
    smart.build(force=False)
    smart.deploy()

    COMMENT('''
    Create test accounts:
    ''')
    create_account("ALICE", MASTER)

    COMMENT('''
    Set value:
    ''')
    HOST.push_action(
        "setval", {"key":42, "value":"thevalue"}, permission=(ALICE, Permission.ACTIVE))


    COMMENT('''
    Get value:
    ''')
    HOST.push_action(
        "getval", {"key":42}, permission=(ALICE, Permission.ACTIVE))
    assert("thevalue" in DEBUG())

    stop()


if __name__ == "__main__":
    test()
