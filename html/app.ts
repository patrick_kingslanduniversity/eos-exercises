ScatterJS.plugins( new ScatterEOS() );

const network = {
    blockchain:'eos',
    protocol:'https',
    host:'api-kylin.eoslaomao.com',
    port:443,
    chainId:'5fff1dae8dc8e2fc4d5b23b2c7665c97f9e9d8edf2b6485a86ba311c25639191'
}

let page = 0;

async function populateTable(eos) {
  $("#pageLabel").text(`Page ${page}`);

  let lb = page * 10;
  let ub = lb + 9;

  const table = await eos.getTableRows({
    code: 'testpsionski',
    scope: 'testpsionski',
    table: 'valstable',
    lower_bound: lb.toString(),
    upper_bound: ub.toString(),
    limit: 10,
    json: true
  });
  console.log(table);

  $('#resTable').empty();
  table.rows.forEach(element => {
    const row = $('<tr>');
    row.append($('<td>').text(element.key));
    row.append($('<td>').text(element.value));
    $('#resTable').append(row);
  });
}

async function addRow(eos, opts) {
  const newKey = Number($('#newKey').val());
  const newVal = $('#newValue').val();
  console.log(`Adding key: ${newKey} value: ${newVal}`);

  const contract = await eos.contract('testpsionski');
  const res = await contract.setval(newKey, newVal, opts);

  $('#newKey').val("");
  $('#newValue').val("");

  return res;
}

ScatterJS.connect('Simple Storage', {network}).then(connected => {
    if(!connected) return console.error('no scatter');

    const eos = ScatterJS.eos(network, Eos);

    ScatterJS.login().then(async id => {
        if(!id) return console.error('no identity');
        const account = ScatterJS.account('eos');
        const options = {authorization:[`${account.name}@${account.authority}`]};

        await populateTable(eos);

        $('#newBtn').click(async _ => {
          const res = await addRow(eos, options);
          console.log('Add row result:', res);
          await populateTable(eos);
        });

        $('#nextPageBtn').click(_ => {
          page++;
          populateTable(eos);
        });

        $('#prevPageBtn').click(_ => {
          if (page > 0) {
            page--;
            populateTable(eos);
          }
        });
    });
});